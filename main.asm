global _start
extern find_word

%include 'library.inc'
%include 'colon.inc'
%include 'words.inc'

section .rodata
	inputMessage: db "Please type a word: ",10, 0
	notFoundError: db "library does not contain this word!",10, 0
	overflowBuffer: db "This word is too long!",10, 0
	
section .text
_start:
	mov rdi, inputMessage
	call print_string
	call read_word
	push rax
	test rax, rax
	jz .buffer_overflow
	pop rax
	mov rsi, pointer
	call find_word
	cmp rax, 0
	je .not_found
	
	add rax, 8
	mov rdi, rax
	call string_length
	add rdi, rax
	inc rdi
	call print_string
	call exit
	
.not_found:
	mov rdi, notFoundError
	call print_error
	jmp exit
.buffer_overflow:
	mov rdi, overflowBuffer
	call print_error
	jmp exit
	


