pre-line = nasm -f elf64 -o

main: main.o lib.o dict.o
			ld -o main main.o dict.o lib.o
%.o : %.asm
			$(pre-line) $@ $<
			 
.PHONY: clean
clean:
			$(RM) .o main
