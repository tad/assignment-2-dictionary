global exit
global string_length
global print_string
global print_char
global print_unit
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_newline
global print_error

%define SYS_EXIT 60
%define SYS_WRITE 1
%define FD_STDOUT 1
%define FD_STDERROR 2
%define MAX_LENGTH 256

section .text

exit: 
    mov rax, SYS_EXIT
    syscall 

string_length:
    xor rax, rax
    .counter:
        cmp byte [rdi + rax], 0
        je .end ;
        inc rax
        jmp .counter
    .end:
    ret

print_string:
    push rdi 
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, FD_STDOUT
    syscall
    ret

print_error:
    push rdi 
    call string_length
    pop rdi 
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rsi, rdi
    mov rdi, FD_STDERROR
    syscall
    ret
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_WRITE
    mov rdi, FD_STDOUT
    syscall
    pop rdi
    ret
    
print_newline:
    mov rdi, 0xA
    jmp print_char

print_uint: 
    push rdx
    mov rbx, 10
    push 0x00 
    push rdi 
    cmp rdi, 0x2D
    je .sign 
    pop rdi
    mov rax, rdi
    .loop:
    	xor rdx, rdx
    	div rbx
    	add rdx, 0x30
    	push rdx
    	cmp rax, rbx
    	jae .loop
    	add rax, 0x30
        cmp rax, 0x30
        je .next
    	push rax
    	xor rax, rax
    .next:
    	pop rdi
    	test rdi, rdi
    	je .eof
    	call print_char
    	jmp .next
    .eof:
    	pop rdx 
    	ret
    .sign: 
    	mov rdi, 0x2D
    	pop rdx
    	jmp print_char
 
print_int:
    mov rbx, rdi
    test rdi, rdi
    jns .end
    mov rdi, '-'
    call print_char
    mov rdi, rbx
    neg rdi
    .end:
    	jmp print_uint

string_equals:
   xor rcx, rcx 
   .loop:
   mov al, [rdi + rcx]
   mov dl, [rsi + rcx]
   cmp al, dl
   jne .neq
   inc rcx
   cmp al, 0
   je .eq
   jmp .loop
    .eq:
    mov rax, 1
    ret
    .neq:
    xor rax, rax
    ret

read_char:
    push 0
    mov rdx, 1
    xor rdi, rdi
    mov rsi, rsp
    xor rax, rax
    syscall
    pop rax
    ret
    
section .data
word_buffer times 256 db 0

section .text
read_word:
    mov r10, word_buffer
    .spaces:
    call read_char
    test rax, rax
    jz .en
    cmp rax, 0x20
    jle .spaces
    
    .next:
    mov [r10], rax
    inc r10
    call read_char
    cmp rax, 32
    jg .next
    
    .en:
    mov rdi, word_buffer
    call string_length
    cmp rax, MAX_LENGTH
    ja .end ; if length is more than 256 then exit
    mov rdx, rax
    mov rax, word_buffer
    ret
    .end:
    xor rdx, rdx
    xor rax, rax
    ret
    
parse_uint:
    xor rcx, rcx
    xor rax, rax 
    xor r8, r8
    mov r10, 10
    .next:
    cmp byte[rdi + rcx], '0'
    jb .end
    cmp byte[rdi + rcx], '9'
    ja .end
    mul r10
    mov r8b, [rdi + rcx]
    sub r8b, '0'
    add rax, r8
    inc rcx
    jmp .next
    .end:
    mov rdx, rcx
    ret

parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

string_copy:
	xor rcx, rcx
	mov r10, rdi
	call string_length
	cmp rax, 19
	ja .end
	mov rdi, r10
	mov cl, [rdi]
	mov [rsi], cl
	inc rdi
	inc rsi
	test rcx, rcx
	jnz string_copy
	
	.end:
	ret

