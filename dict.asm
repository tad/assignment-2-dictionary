global find_word
extern string_equals

section .text

;rdi: pointer to a null-terminated string
;rsi: pointer to the beginning of dictionary
find_word: 
	.loop:
		test rsi, rsi
		jz .notFound
		push rdi
		push rsi
		call string_equals
		cmp rax, 1
		je .found
		pop rsi
		pop rdi
		add rsi, 8
		mov rsi, [rsi] ; get next word 
		jmp .loop
	
	.found:
		mov rax, rsi
		ret
	.notFound:
		xor rax, rax
		ret
	
	
